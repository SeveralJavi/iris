﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextBoxManager : MonoBehaviour
{
    public GameObject textBox;
    public Text theText;
    public Text electionText1;
    public Text electionText2;
    public GameObject textBoxManager;

    public TextAsset textFile;
    private string[] textLines;

    private int currentLine;
    private int endAtLine;
    private int electionNumber = 0;

    private string displayedText = "";
    public string textToDisplay;
    private bool showingText = false;
    private bool animationIsPlaying = false;
    private bool electionIsWorking = false;

    private float delay = 0.05f;
    private bool displayingVideo = false;

    private Sprite BGsSpriteLeft;
    private Sprite BGsSpriteRight;

    private Sprite globoDialogo;

    private Sprite speakerSpriteLeft;
    private Sprite speakerSpriteRight;

    private Sprite happyConsumistSprite;
    private Sprite sadConsumistSprite;
    private Sprite seriousConsumistSprite;
    private Sprite neutralConsumistSprite;
    private Sprite disappointedConsumistSprite;
    private Sprite surprisedConsumistSprite;

    private Sprite happyUserSprite;
    private Sprite sadUsertSprite;
    private Sprite seriousUserSprite;
    private Sprite neutralUserSprite;
    private Sprite disappointedUserSprite;

    private Sprite happyFatherSprite;
    private Sprite sadFatherSprite;
    private Sprite seriousFatherSprite;
    private Sprite neutralFatherSprite;
    private Sprite disappointedFatherSprite;

    private bool isTalkingLeft = true;

    private Sprite backgroundSprite;
    private Sprite BGBACKROOMONE;
    private Sprite BGBACKROOMTWO;
    private Sprite SEQUENCEONETVONE;
    private Sprite SEQUENCEONETVTWO;
    private Sprite SEQUENCETWO;
    private Sprite SEQUENCETHREE;
    private Sprite SEQUENCEBLACK;
    private Sprite SEQUENCEWHITE;
    private Sprite BGKITCHEN;
    private Sprite STREETONE;


    public Font font1;
    public Font font2;
    public Font font3;

    public VideoManager myVideoManager;
    public GameObject myAudioManagerGO;
    public GameObject myVideoManagerGO;
    public GameObject Comic1;
    public GameObject Comic2;

    public string myTVText;

    // Start is called before the first frame update

    void Awake(){
        speakerSpriteLeft = GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite;
        speakerSpriteRight = GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite;
        globoDialogo = GameObject.Find("SpeechBalloon").GetComponent<Image>().sprite;

        BGsSpriteLeft = GameObject.Find("BGSpeakerLeft").GetComponent<Image>().sprite;
        BGsSpriteRight = GameObject.Find("BGSpeakerRight").GetComponent<Image>().sprite;

        if (GameObject.Find("ElectionText1") != null) electionText1 = GameObject.Find("ElectionText1").GetComponent<Text>();
        if (GameObject.Find("ElectionText2") != null) electionText2 = GameObject.Find("ElectionText2").GetComponent<Text>();
        
        //BGs
        backgroundSprite = GameObject.Find("Background").GetComponent<Image>().sprite;
        BGBACKROOMONE = Resources.Load<Sprite>("Scenarios/TrastiendaV2");
        BGBACKROOMTWO = Resources.Load<Sprite>("Scenarios/Trastienda");
        SEQUENCEONETVONE = Resources.Load<Sprite>("Scenarios/television1");
        SEQUENCEONETVTWO = Resources.Load<Sprite>("Scenarios/television2");
        SEQUENCETWO = Resources.Load<Sprite>("Scenarios/Habitacion");
        SEQUENCETHREE = Resources.Load<Sprite>("Scenarios/Cocina-ComedorSInMesa");
        SEQUENCEBLACK = Resources.Load<Sprite>("Scenarios/Negro");
        SEQUENCEWHITE = Resources.Load<Sprite>("Scenarios/Blanco");
        BGKITCHEN = Resources.Load<Sprite>("Scenarios/Cocina-ComedorSInMesa");
        STREETONE = Resources.Load<Sprite>("Scenarios/CalleGenéricaVersionFinal");

        //Padre
        Sprite[] fatherSprites = Resources.LoadAll<Sprite>("DialogSpr/Spritesheet_Padre");
        neutralFatherSprite = fatherSprites[0];
        sadFatherSprite = fatherSprites[1];
        seriousFatherSprite = fatherSprites[3];
        happyFatherSprite = fatherSprites[4];
        disappointedFatherSprite = fatherSprites[2];

        //Usuaria
        Sprite[] userSprites = Resources.LoadAll<Sprite>("DialogSpr/Spritesheet_Usuaria");
        neutralUserSprite = userSprites[0];
        sadUsertSprite = userSprites[1];
        seriousUserSprite = userSprites[2];
        happyUserSprite = userSprites[3];
        disappointedUserSprite = userSprites[4];
        
        if (GameObject.Find("UsuariaOnBG_Standing") != null) GameObject.Find("UsuariaOnBG_Standing").GetComponent<Image>().enabled = false;
        if (GameObject.Find("UsuariaOnBG_Sitting") != null) GameObject.Find("UsuariaOnBG_Sitting").GetComponent<Image>().enabled = false;

        //Empresaria
        Sprite[] consumistSprites = Resources.LoadAll<Sprite>("DialogSpr/Spritesheet_Empresaria");
        happyConsumistSprite = consumistSprites[10];
        sadConsumistSprite = consumistSprites[2];
        seriousConsumistSprite = consumistSprites[4];
        neutralConsumistSprite = consumistSprites[0];
        disappointedConsumistSprite = consumistSprites[6];
        surprisedConsumistSprite = consumistSprites[8];

        if (GameObject.Find("EmpresariaOnBG_Standing") != null) GameObject.Find("EmpresariaOnBG_Standing").GetComponent<Image>().enabled = false;
        if (GameObject.Find("EmpresariaOnBG_Sitting") != null) GameObject.Find("EmpresariaOnBG_Sitting").GetComponent<Image>().enabled = false;

        //Texto TV

        myTVText = GameObject.Find("TVText").GetComponent<Text>().text;
        GameObject.Find("TVText").GetComponent<Text>().text = "";

        //Escenas Comic

        Comic1 = GameObject.Find("Comic1");
        Comic2 = GameObject.Find("Comic2");
    }

    void Start()
    {
        if (textFile != null) textLines = (textFile.text.Split('\n'));
        if (textFile != null && endAtLine == 0) endAtLine = textLines.Length - 1;

        myVideoManager = FindObjectOfType<VideoManager>();
        myVideoManagerGO = GameObject.Find("VideoManager");
        myVideoManagerGO.SetActive(false);
    }



    // Update is called once per frame
    void Update()
    {
        if (textBox.activeSelf && !displayingVideo)
        {
            if (Input.GetMouseButtonDown(0) && !animationIsPlaying && !electionIsWorking)
            {
                StopAllCoroutines();
                showingText = false;

                if (currentLine >= endAtLine)
                {

                    textBox.SetActive(false);
                    textBoxManager.SetActive(false);
                    Reset();

                }
                else
                {
                    currentLine += 1;
                }
            }

            textToDisplay = textLines[currentLine];

            if (textToDisplay == "") {  // CASOS ESPECIALES: Cadena vacía
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }

            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "FADETOWHITEOUT")
            {
                GameObject.Find("VideoBase").GetComponent<Animation>().Play("FADETOWHITEOUT");
                currentLine += 1;
                textToDisplay = textLines[currentLine];

            }

            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "FADETOWHITEIN")
            {
                GameObject.Find("VideoBase").GetComponent<Animation>().Play("FADETOWHITEIN");
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }                    

            //Para cambiar de escena. Se pone el CHANGESCENE y abajo el nombre de la escena (sin espacios ni bajar la linea que estoy mirando pero de momento se raya 
            //por tema de espacios seguramente
            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "CHANGESCENE")
            {
                currentLine += 1;
                string changescene = textLines[currentLine];
                SceneManager.LoadScene(changescene);
            }

            //he puesto si lenght > 5 para que no se raye si intenta comprobar alguna substring mas corta
            //IMPORTANTE: Al poner POS XY tienen que ser tres cifras en la coord x e y, si quieres poner de coordx 10 pongamos 010 o esto se rayará

            else if (textToDisplay.Length>5 && textToDisplay.Substring(0, 6) == "POS XY"){ // CASOS ESPECIALES: Modificar posición del texto

                //Debug.Log("POS XY");
                //Debug.Log("textToDisplay: "+textToDisplay);

                string coordx = textToDisplay.Substring(7, 3);
                string coordy = textToDisplay.Substring(11, 3);
                int a = int.Parse(coordx);      
                int b = int.Parse(coordy);

                
                //Debug.Log("a (substring 7,3): " + a + "b(substr 11,3): " + b);

                //Esta es una función que he puesto abajo que hace lo del nuevo vector etc
                ChangePosition(a, b);

                //DEJO ESTO EN COMENTADO POR SI ACASO
                //string newXY = textToDisplay.Substring(0, textToDisplay.Length - 1);
                //string newY = textToDisplay.Substring(textToDisplay.Length - 4, textToDisplay.Length - 1);

                // GameObject.Find("Text").GetComponent<RectTransform>().anchoredPosition = new Vector2(250, 150);
                // PROVISIONAL
                //= new Vector2(float.Parse(textToDisplay.Substring(textToDisplay.Length-8, textToDisplay.Length-5)), float.Parse(textToDisplay.Substring(textToDisplay.Length-4, textToDisplay.Length-1)));            
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }

            else if (textToDisplay.Length>15 && textToDisplay.Substring(0, 16) == "SPEECHBOX POS XY"){ // CASOS ESPECIALES: Modificar posición del globo de texto

                string coordx = textToDisplay.Substring(17, 3);
                string coordy = textToDisplay.Substring(21, 3);

                GameObject.Find("SpeechBalloon").GetComponent<RectTransform>().anchoredPosition = new Vector2(int.Parse(coordx), int.Parse(coordy));
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }
            
            //else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "POS XY 250 250"){ // Provisional porque no funciona bien
                
            //    GameObject.Find("Text").GetComponent<RectTransform>().anchoredPosition = new Vector2(200, -30);
            //    currentLine += 1;
            //    textToDisplay = textLines[currentLine];
            //}

            else if (textToDisplay.Length>13 && textToDisplay.Substring(0, 14) == "SALTO A LINEA "){ // CASOS ESPECIALES: Salto de línea
                JumpToLine(int.Parse(textToDisplay.Substring(15, 3)) - 1);
            }

            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "ANCHORLEFT"){
                GameObject.Find("Text").GetComponent<Text>().alignment = TextAnchor.UpperLeft;
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }

            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "ANCHORCENTER"){
                GameObject.Find("Text").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }

            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "COLORBLACK"){ // CASOS ESPECIALES: Cambiar el color
                GameObject.Find("Text").GetComponent<Text>().color = Color.black;
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }

            else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "ANIMTVON"){ // CASOS ESPECIALES: Animación de overlay
                myVideoManagerGO.SetActive(true);
                myVideoManager.myVideoBase.GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                myVideoManager.StartLoad();
                displayingVideo = true;
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }

            // Para esperar

/*
            else if (textToDisplay.Length > 5 && textToDisplay.Substring(0, 4) == "WAIT"){
                StopCoroutine(ShowText());
                float timer = 0;
                float timeToReach = float.Parse(textToDisplay.Substring(5, 4));

                while(timer < timeToReach){
                    timer += (float)0.01;
                    Debug.Log("esperando");
                }
                StartCoroutine(ShowText());
                Debug.Log("ESPERADO");
                currentLine += 1;
                textToDisplay = textLines[currentLine];
            }*/

            // OTROS CASOS ESPECIALES

            else if(IsAllUpper(textToDisplay.Substring(0, textToDisplay.Length-1))){

                // Para conocer qué lado está hablando

                if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "LEFTTALKER"){
                    isTalkingLeft = true;
                    GameObject.Find("SpeechBalloon").GetComponent<Image>().sprite = Resources.Load<Sprite>("DialogSpr/GloboDialogoLeft");
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "RIGHTTALKER"){
                    isTalkingLeft = false;
                    GameObject.Find("SpeechBalloon").GetComponent<Image>().sprite = Resources.Load<Sprite>("DialogSpr/GloboDialogoRight");
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "NEUTRALTALKER"){
                    GameObject.Find("SpeechBalloon").GetComponent<Image>().sprite = Resources.Load<Sprite>("DialogSpr/GloboDialogoNeutral");
                }
                // Para la escena de BG
                if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "BGBACKROOMONE")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = BGBACKROOMONE;
                    GameObject.Find("UsuariaOnBG_Standing").GetComponent<Image>().enabled = true;
                    GameObject.Find("EmpresariaOnBG_Sitting").GetComponent<Image>().enabled = true;
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "BGBACKROOMTWO")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = BGBACKROOMTWO;
                    GameObject.Find("UsuariaOnBG_Standing").GetComponent<Image>().enabled = false;
                    GameObject.Find("EmpresariaOnBG_Sitting").GetComponent<Image>().enabled = false;
                    GameObject.Find("EmpresariaOnBG_Standing").GetComponent<Image>().enabled = true;
                    GameObject.Find("UsuariaOnBG_Sitting").GetComponent<Image>().enabled = true;
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "BGKITCHEN")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = BGKITCHEN;
                    GameObject.Find("Table").GetComponent<Image>().enabled = true;
                    GameObject.Find("UserOnBG_Sitting").GetComponent<Image>().enabled = false;
                    GameObject.Find("PadreOnBG_Sitting").GetComponent<Image>().enabled = false;
                    GameObject.Find("UserOnBG_Standing").GetComponent<Image>().enabled = true;
                    GameObject.Find("PadreOnBG_Standing").GetComponent<Image>().enabled = true;
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "BGKITCHENTWO")
                {
                    GameObject.Find("UserOnBG_Sitting").GetComponent<Image>().enabled = true;
                    GameObject.Find("PadreOnBG_Sitting").GetComponent<Image>().enabled = true;
                    GameObject.Find("UserOnBG_Standing").GetComponent<Image>().enabled = false;
                    GameObject.Find("PadreOnBG_Standing").GetComponent<Image>().enabled = false;
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCEONETVONE")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCEONETVONE;
                    //GameObject.Find("Background").GetComponent<Animation>().Play("Sequence01Spawn");

                    LoopingText.textToLoop = myTVText;

                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCEONETVTWO")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCEONETVTWO;
                    GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonSlideDown");
                    GameObject.Find("Text").GetComponent<Animation>().Play("TextSlideDown");

                    GameObject.Find("TVText").GetComponent<Text>().text = "";
                    LoopingText.textToLoop = "";

                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCEONETVOUT")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCEONETVTWO;
                    GameObject.Find("Background").GetComponent<Animation>().Play("Sequence01Zoom");
                }

               
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCETWO")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCETWO;
                    GameObject.Find("UserMusic_Sitting").GetComponent<Image>().enabled = true;
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCETHREE")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCETHREE;
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCEBLACK")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCEBLACK;
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SEQUENCEWHITE")
                {
                    GameObject.Find("Background").GetComponent<Image>().sprite = SEQUENCEWHITE;
                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SPEECHBOXON")
                {
                    //GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonIntro");
                    //currentLine += 1;
                    //textToDisplay = textLines[currentLine];
                    showingText = false;

                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SPEECHBOXONANIMATED")
                {
                    GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonIntro");
                    showingText = false;

                }
                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SPEECHBOXOFF")
                {
                    GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonOutro");
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "CHARACTERBOXON")
                {
                    GameObject.Find("BGSpeakerLeft").GetComponent<Animation>().Play("SpeakerLeftIn");
                    GameObject.Find("BGSpeakerRight").GetComponent<Animation>().Play("SpeakerRightIn");
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "CHARACTERBOXOFF")
                {
                    GameObject.Find("BGSpeakerLeft").GetComponent<Animation>().Play("SpeakerLeftOut");
                    GameObject.Find("BGSpeakerRight").GetComponent<Animation>().Play("SpeakerRightOut");
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "COMICON")
                {
                    Comic1.GetComponent<Animation>().Play("Comic1SlideIn");
                    Comic2.GetComponent<Animation>().Play("Comic2SlideIn");
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "COMICOFF")
                {
                    //Comic1.SetActive(false);

                    //Comic1.GetComponent<Image>().sprite = null;
                    //Comic1.GetComponent<Image>().sprite = null;
                }

                // Para esperar a la animación

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "WAITFORANIMATION")
                {
                    textToDisplay = "";
                    if (Comic1.GetComponent<Animation>().isPlaying ||
                        Comic2.GetComponent<Animation>().isPlaying ||
                        GameObject.Find("BGSpeakerLeft").GetComponent<Animation>().isPlaying ||
                        GameObject.Find("BGSpeakerRight").GetComponent<Animation>().isPlaying ||
                        GameObject.Find("VideoBase").GetComponent<Animation>().isPlaying ||
                        GameObject.Find("Background").GetComponent<Animation>().isPlaying ||
                        GameObject.Find("SpeechBalloon").GetComponent<Animation>().isPlaying)
                    {
                        animationIsPlaying = true;
                    }
                    else animationIsPlaying = false;
                }
                // Para conocer las emociones del emisor o receptor

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SADCONSUMIST")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = sadConsumistSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = sadConsumistSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "HAPPYCONSUMIST")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = happyConsumistSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = happyConsumistSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "NEUTRALCONSUMIST")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = neutralConsumistSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                             GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = neutralConsumistSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SERIOUSCONSUMIST")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = seriousConsumistSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = seriousConsumistSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "DISAPPOINTEDCONSUMIST")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = disappointedConsumistSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = disappointedConsumistSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SURPRISEDCONSUMIST")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 21 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = surprisedConsumistSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 22) == "Spritesheet_Empresaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = surprisedConsumistSprite;
                    }
                }


                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "HAPPYUSER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 18 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = happyUserSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = happyUserSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SADUSER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 18 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = sadUsertSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = sadUsertSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "NEUTRALUSER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 18 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = neutralUserSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = neutralUserSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SERIOUSUSER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 18 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = seriousUserSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = seriousUserSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "DISAPPOINTEDUSER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 18 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = disappointedUserSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 19) == "Spritesheet_Usuaria")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = disappointedUserSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "HAPPYFATHER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 16 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = happyFatherSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = happyFatherSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SADFATHER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 16 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = sadFatherSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 18) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = sadFatherSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "NEUTRALFATHER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 16 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = neutralFatherSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = neutralFatherSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "SERIOUSFATHER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 16 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = seriousFatherSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = seriousFatherSprite;
                    }
                }

                else if (textToDisplay.Substring(0, textToDisplay.Length - 1) == "DISAPPOINTEDFATHER")
                {
                    if (GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Length > 16 &&
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteLeft").GetComponent<Image>().sprite = disappointedFatherSprite;
                    }
                    else if (GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite.name.Substring(0, 17) == "Spritesheet_Padre")
                    {
                        GameObject.Find("SpeakerSpriteRight").GetComponent<Image>().sprite = disappointedFatherSprite;
                    }
                }

                if(!animationIsPlaying && !electionIsWorking){
                    currentLine += 1;
                    textToDisplay = textLines[currentLine];
                }
            }

            // SISTEMA DE ELECCIONES

            else if(textToDisplay.Length > 10 && textToDisplay.Substring(0, 11) == "ELECCION 1 "){
                currentLine += 1;
                electionText1.text = textToDisplay.Substring(11, textToDisplay.Length - 11);
                textToDisplay = "...";
            }

            else if(textToDisplay.Length > 10 && textToDisplay.Substring(0, 11) == "ELECCION 2 "){
                currentLine += 1;
                electionText2.text = textToDisplay.Substring(11, textToDisplay.Length - 11);

                GameObject.Find("ElectionBalloon1").GetComponent<Animation>().Play("ElectionBalloonIntro");
                GameObject.Find("ElectionBalloon2").GetComponent<Animation>().Play("ElectionBalloonIntro");
                GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonTranslucentOn");
                electionIsWorking = true;
            }
            
            // El texto no está siendo animado; comienza la animación

            if (!showingText){
                showingText = true;
                StartCoroutine(ShowText());
                StartCoroutine(ChangeTextFont());
            }
        }

        if((textToDisplay.Length > 0 && textToDisplay.Substring(0, textToDisplay.Length - 1) == displayedText)) myAudioManagerGO.GetComponent<AudioSource>().mute = true;
        else myAudioManagerGO.GetComponent<AudioSource>().mute = false;

        if(myVideoManager != null && myVideoManager.loops != 0){
            myVideoManager.myVideoBase.GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
            myVideoManagerGO.SetActive(false);
            displayingVideo = false;
        }
    
    }

    bool IsAllUpper(string input) // Detecta si toda la cadena está en mayúsculas (= cambios del entorno)
    {
    for (int i = 0; i < input.Length; i++)
    {
        if (!char.IsUpper(input[i]))
             return false;
    }

    return true;
    }

    IEnumerator ShowText(){
        for (int i = 0; i < textToDisplay.Length; i++)
        {
            displayedText = textToDisplay.Substring(0,i);
            theText.text = displayedText;
            yield return new WaitForSeconds(delay);
        }
    }

    IEnumerator ChangeTextFont(){
        if(GameObject.Find("Text").GetComponent<Text>().font == font1){
            GameObject.Find("Text").GetComponent<Text>().font = font2;
            if (GameObject.Find("ElectionText1") != null) GameObject.Find("ElectionText1").GetComponent<Text>().font = font2;
            if (GameObject.Find("ElectionText1") != null) GameObject.Find("ElectionText2").GetComponent<Text>().font = font2;
            //GameObject.Find("Text").GetComponent<Text>().fontStyle = FontStyle.Bold;
        }
        else if(GameObject.Find("Text").GetComponent<Text>().font == font2){
            GameObject.Find("Text").GetComponent<Text>().font = font3;
            if (GameObject.Find("ElectionText1") != null) GameObject.Find("ElectionText1").GetComponent<Text>().font = font3;
            if (GameObject.Find("ElectionText1") != null) GameObject.Find("ElectionText2").GetComponent<Text>().font = font3;
        }
        else{
            GameObject.Find("Text").GetComponent<Text>().font = font1;
            if (GameObject.Find("ElectionText1") != null) GameObject.Find("ElectionText1").GetComponent<Text>().font = font1;
            if (GameObject.Find("ElectionText1") != null) GameObject.Find("ElectionText2").GetComponent<Text>().font = font1;
            //GameObject.Find("Text").GetComponent<Text>().fontStyle = FontStyle.Normal;
        }

        yield return new WaitForSeconds(0.3f);
        StartCoroutine(ChangeTextFont());
    }

    void Reset() //en caso de que quiera que el texto vuelva a salir si le vuelves a dar
    {
        currentLine = 0;
    }

    public void JumpToLine(int selectedLine){
        currentLine = selectedLine;
        textToDisplay = textLines[currentLine];
    }

    public void clickOnElectionBalloon1(){
        if (electionNumber == 0) JumpToLine(27);
        if (electionNumber == 1) JumpToLine(31);

        GameObject.Find("ElectionBalloon1").GetComponent<Animation>().Play("ElectionBalloonOutro");
        GameObject.Find("ElectionBalloon2").GetComponent<Animation>().Play("ElectionBalloonOutro");
        GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonTranslucentOff");

        electionNumber++;
        electionIsWorking = false;
        textToDisplay = textLines[currentLine];
        StartCoroutine(ShowText());
    }

    public void clickOnElectionBalloon2(){
        if (electionNumber == 0) JumpToLine(36);
        if (electionNumber == 1) JumpToLine(54);

        GameObject.Find("ElectionBalloon1").GetComponent<Animation>().Play("ElectionBalloonOutro");
        GameObject.Find("ElectionBalloon2").GetComponent<Animation>().Play("ElectionBalloonOutro");
        GameObject.Find("SpeechBalloon").GetComponent<Animation>().Play("SpeechBalloonTranslucentOff");

        electionNumber++;
        electionIsWorking = false;
        textToDisplay = textLines[currentLine];
        StartCoroutine(ShowText());
    }

    void ChangePosition(int a, int b)
    {
        GameObject.Find("Text").GetComponent<RectTransform>().anchoredPosition = new Vector2(a, b);
        return;

    }

    public void ChangeTextAsset(string route)
    {
        textFile = Resources.Load<TextAsset>("DialogSpr/Spritesheet_Usuaria");

    }
}

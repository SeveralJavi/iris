﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public RawImage rawImage;
    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    public GameObject myVideoBase;
    public int loops = 0;

    // Start is called before the first frame update
    void Awake()
    {
        myVideoBase = GameObject.Find("VideoBase");
        myVideoBase.GetComponent<RawImage>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        myVideoBase.GetComponent<Animation>().Play("FADETOWHITEOUT");
        //StartCoroutine(PlayVideo());

        videoPlayer.loopPointReached += EndReached;
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        loops++;
    }

    public void StartLoad(){
        StartCoroutine(PlayVideo());
    }

    public IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(0.1f);
        while (!videoPlayer.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }

        myVideoBase.GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
        audioSource.Play();
        /*
        while(!videoPlayer.isPlaying){
            Debug.Log("tamos activo");
            yield return waitForSeconds;
            break;
        }*/

        /*
        myVideoBase.SetActive(false);
        gameObject.SetActive(false);
        */
    }
}

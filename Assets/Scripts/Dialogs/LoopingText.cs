﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoopingText : MonoBehaviour
{
    public static string textToLoop;
    public float waitTime = 2.0f;
    private float timer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        textToLoop = gameObject.GetComponent<Text>().text;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (textToLoop.Length > 0 && timer > waitTime)
        {
            string letterToPop = textToLoop.Substring(0, 1);
            string newStart = textToLoop.Substring(1, textToLoop.Length - 1);
            textToLoop = newStart + letterToPop;
            gameObject.GetComponent<Text>().text = textToLoop;

            timer = timer - waitTime;
        }

        //GameObject.Find("SpeechBalloon").GetComponent<RectTransform>().anchoredPosition = new Vector2(int.Parse(coordx), int.Parse(coordy));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour
{

     [SerializeField]

    public Sprite cursor;

    private SpriteRenderer rend;

    public string actualScene;

    private GameController GC;

    private Scene SCENE;

    public GameController xd;

 




    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;           // Oculta el cursor predeterminado de cara a mostrar el personalizado
        rend = GetComponent<SpriteRenderer>();

       
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);    //Cogemos las coordenadas del ratón
        transform.position = cursorPos;

    }
    
    public void endPause()
    {
        SceneManager.UnloadSceneAsync("TangramMenuPause" , UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }

    public void resetTangram()                                                    //Este metodo recarga el Tangram actual
    {
        SceneManager.LoadScene(actualScene, LoadSceneMode.Single);


    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public Font font1;
    public Font font2;
    public Font font3;

    public Text myText;
    public Text buttonText1;
    public Text buttonText2;
    public Text buttonText3;
    public bool english = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ChangeTextFont());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ChangeTextFont(){
        if(myText.font == font1){
            myText.font = font2;
            buttonText1.font = font2;
            buttonText2.font = font2;
            buttonText3.font = font2;
            //GameObject.Find("Text").GetComponent<Text>().fontStyle = FontStyle.Bold;
        }
        else if(myText.font == font2){
            myText.font = font3;
            buttonText1.font = font3;
            buttonText2.font = font3;
            buttonText3.font = font3;
        }
        else{
            myText.font = font1;
            buttonText1.font = font1;
            buttonText2.font = font1;
            buttonText3.font = font1;
        }

        yield return new WaitForSeconds(0.2f);
        StartCoroutine(ChangeTextFont());
    }

    public void StartGame(){
        if (!GameObject.Find("VideoBase").GetComponent<Animation>().isPlaying){
            GameObject.Find("VideoBase").GetComponent<Animation>().Play("FADETOWHITEIN");
            StartCoroutine(FadeToWhiteRoutine());
        }
    }

    IEnumerator FadeToWhiteRoutine(){
        if (!GameObject.Find("VideoBase").GetComponent<Animation>().isPlaying) SceneManager.LoadScene("Scene1");
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(FadeToWhiteRoutine());
    }

    public void ExitGame(){
        Application.Quit();
    }

    public void ChangeLanguage(){
        if (english == false){
            english = true;

            buttonText1.text = "Change language";
            buttonText2.text = "Start game";
            buttonText3.text = "Quit game";
        }
        else{
            english = false;

            buttonText1.text = "Cambiar idioma";
            buttonText2.text = "Comenzar partida";
            buttonText3.text = "Salir del juego";
        }
    }
}

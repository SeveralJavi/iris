﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorationController : MonoBehaviour
{
    [SerializeField]
    public bool onMoveMode = false;

    private SpriteRenderer rend;
    public Sprite moveCursor;

    public GameObject clickEffect;

    void Start()
    {
        Cursor.visible = false; // Oculta el cursor predeterminado de cara a mostrar el personalizado
        rend = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPos;

        if (onMoveMode) rend.sprite = moveCursor;

        if(Input.GetMouseButtonDown(0)){
            GameObject newClickEffect = (GameObject)Instantiate(clickEffect, transform.position, Quaternion.identity);
            Destroy(newClickEffect, 1);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]

                                                    //Booleanos que nos marcan en que modo esta el Tangram
    public bool onCropMode = false;               //Modo Cortar
    public bool onRotateMode = false;             //Modo Rotar
    public bool onMoveMode = false;               //Modo Mover
    public bool onJoinMode = false;               //Modo Unit
    private string mode;

    private SpriteRenderer rend;                  //Sprites de cada cursor dependiendo del modo que este en Tangram
    public Sprite cropCursor;                       
    public Sprite rotateCursor;
    public Sprite moveCursor;
    public Sprite joinCursor;

    public Text UIText;                           //Para mostrar el cronometro
    public float time = 0;
    public bool continueTime = true;

    public string actualScene;                   //Este string es para saber en que nivel del Tangram nos encontramos, asi cuando se resetee el Tangram el juego sepa que escena tiene que reiniciar.

    public static GameController GC;             //Esto es para que el cronometro no se ponga a 0 cuando se reinicie la escena (ya que el cronómetro se encuentra en este script)

    public GameObject buttons;



    private void Awake()
    {
        MakeThisTheOnlyGameController();           
      
    }


    void Start()
    {
        
        
        Cursor.visible = false;                // Oculta el cursor predeterminado de cara a mostrar el personalizado
        rend = GetComponent<SpriteRenderer>();
        
    }

    void Update()
    {
        
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);    //Cogemos las coordenadas del ratón
        transform.position = cursorPos;

        if (onCropMode) rend.sprite = cropCursor;                                   //Para poner el cursor correcto
        if (onRotateMode) rend.sprite = rotateCursor;
        if (onMoveMode) rend.sprite = moveCursor;


        if(continueTime)
            time += Time.deltaTime;                                                 //Cronómetro, suma el tiempo
        UIText.text = Mathf.Round(time).ToString();                                 //Muestra en pantalla el tiempo actual.

        if (SceneManager.GetSceneByName("TangramMenuPause").isLoaded) {             //Este if solo tiene la funcion de que cuando el menu pause este activo no se puedan seguir desplazando , rotando , etc... las piezas.
            continueTime = false;
            if (onCropMode)
            {
                mode = "onCropMode";
                onCropMode = false;

            }
            if (onRotateMode)
            {
                mode = "onRotateMode";
                onRotateMode = false;
            }
            if (onMoveMode)
            {
                mode = "onMoveMode";
                onMoveMode = false;
            }
            if (onJoinMode)
            {
                mode = "onJoinMode";
                onJoinMode = false;
            }
            Debug.Log(mode);
            buttons.SetActive(false);
        }
        else
        {
            continueTime = true;
        }
        
        if(!onCropMode  && !onRotateMode  && !onMoveMode && !onJoinMode)   //Este if sirve para cuando se desactiva el menu Pause vuelva otra vez al modo que estaba seleccionado anteriormente
        {
            if (continueTime)
            {
                switch (mode)
                {
                    case "onCropMode":
                        onCropMode = true;
                        break;
                    case "onRotateMode":
                        onRotateMode = true;
                        break;
                    case "onMoveMode":
                        onMoveMode = true;
                        break;
                    case "onJoinMode":
                        onJoinMode = true;
                        break;
                }

                buttons.SetActive(true) ;


            }
        }
    }
    
    public void activateCrop(){                                                     //Activar modo Cortar
        onCropMode = true;
        onRotateMode = false;
        onMoveMode = false;
        onJoinMode = false;
    }
    
    public void activateRotate(){                                                   //Activar modo Rotar
        onCropMode = false;
        onRotateMode = true;
        onMoveMode = false;
        onJoinMode = false;
    }

    public void activateMovement(){                                                 //Activar modo Mover
        onCropMode = false;
        onRotateMode = false;
        onMoveMode = true;
        onJoinMode = false;
    }
    
    public void activateJoin()                                                      //Activar modo Unir
    {
        onCropMode = false;
        onRotateMode = false;
        onMoveMode = false;
        onJoinMode = true;

    }
    public void pauseTangram()                                          
    {
        continueTime = false;
        SceneManager.LoadScene("TangramMenuPause", LoadSceneMode.Additive);
    }
     
    private void MakeThisTheOnlyGameController()                                    //Este metodo permite que a la hora de reinicar el Tangram el cronometro no se duplique.
    {
       if(GC == null)
        {
            DontDestroyOnLoad(gameObject);
            GC = this;
        }
        else
        {
            if(GC != this)
            {
                Destroy(gameObject);
            }
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private UI_Inventory uiInventory = null;

    private Vector2 target; //el vector de la posicion del ratón

    private Inventory inventory;
    private UI_Inventory UI_inventory;
    private Collider2D neopreneCollider;

    public ItemAssets itemAssets;
    private Item item;

    public void Start(){
        inventory = new Inventory(UseItem);
        uiInventory.SetPlayer(this);
        uiInventory.SetInventory(inventory);

        ItemWorld.SpawnItemWorld(new Vector3(200, -100), new Item{ itemType = Item.ItemType.Coin, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(100, -50), new Item{ itemType = Item.ItemType.Coin, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(0, -100), new Item{ itemType = Item.ItemType.HairTie, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(0, -200), new Item{ itemType = Item.ItemType.Juggle, amount = 1 });
    }

    private void UseItem(Item item){
        //Debug.Log("Use item Player controller " +item + " " + item.itemType);
        switch (item.itemType)
        {
            //Por alguna razón ahora al hacer el Remove se congela el Unity aunque no he tocado nada de ahí lol
            case Item.ItemType.Coin:
                Debug.Log("Has usado una moneda");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.Coin, amount = 1 });
                break;
            case Item.ItemType.Neoprene:
                Debug.Log("Has usado neopreno");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.Neoprene, amount = 1 });
                break;
            case Item.ItemType.Wool:
                Debug.Log("Has usado wool");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.Wool, amount = 1 });
                break;
            case Item.ItemType.HairTie:
                Debug.Log("Has usado una goma del pelo");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.HairTie, amount = 1 });
                break;
            case Item.ItemType.Juggle:
                Debug.Log("Has usado pelota");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.Juggle, amount = 1 });
                break;
            case Item.ItemType.LargeCan:
                Debug.Log("Has usado lata");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.LargeCan, amount = 1 });
                break;
            case Item.ItemType.Stick:
                Debug.Log("Has usado palo");
                inventory.RemoveItem(new Item { itemType = Item.ItemType.Stick, amount = 1 });
                break;
        }
    }

    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //coge la posicion que has clicado


        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        if (Input.GetMouseButtonDown(0) && hit.collider != null ){
           

            GameObject collisionItem = hit.collider.gameObject;
            Debug.Log("collision item "+collisionItem);

            if (hit.collider != null && collisionItem == GameObject.Find("ItemSlotTemplate(Clone)")){

                //Esta parte he conseguido que funcione y la he pasado al DragDrop//

                //if(hit.collider.gameObject.GetComponentInChildren<Sprite>() == ) //----> ESTO ES LO CONFLICTIVO
                //  GetChildWithName(GameObject obj, string name)
               
                Sprite itemSprite = collisionItem.transform.Find("Image").GetComponent<Sprite>();
                Debug.Log("itemSprite golpeado es "+itemSprite);


               

            }
           
            else if(hit.collider != null)
            {
                ItemWorld itemWorld = hit.collider.GetComponent<ItemWorld>();
                if(itemWorld != null){
                    inventory.AddItem(itemWorld.GetItem());
                    itemWorld.DestroySelf();
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; //para cambiar de escena



public class PointAndClick : MonoBehaviour
{
    private Vector2 target; //el vector de la posicion del ratón

    public GameObject door = null;//objeto que luego hay que especificar

    public GameObject textBox = null;//texto basicamente

    public Text text;

    public GameObject TextManager;

    private Sprite otherScene;
    private Sprite STREETONE;
    private ItemWorld item;

    public GameObject menuButton = null;
    public GameObject UiInventory;
    public GameObject walkButton;

    bool walk = false; 

    //vale, mi idea seria conseguir especificar luego el texto 
    //de forma que se pueda decir al textboxmanager qué textasset tiene que cargar
    //(dando la ruta)
    //y que por ejemplo pues en el texto ponga donde tiene que ir situado el texto etc


    void Awake()
    {
        STREETONE = Resources.Load<Sprite>("Scenarios/CalleGenéricaVersionFinal"); 

    }

    void Start()
    {
        textBox.SetActive(false);      

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //coge la posicion que has clicado

        //collider de los distintos objetos


        BoxCollider2D collision = door.GetComponent<BoxCollider2D>();
        BoxCollider2D menuColl = menuButton.GetComponent<BoxCollider2D>();
        BoxCollider2D walkColl = walkButton.GetComponent<BoxCollider2D>();

        //Añadir el boxcollider al boton de andar y añadir un if que cambie "walk" a true/false cuando das click

        if(walkColl.OverlapPoint(mousePos)&& Input.GetMouseButtonDown(0))
        {
            if (walk) { walk = false; }
            else { walk = true; }
        }

        //puerta
        if (collision.OverlapPoint(mousePos) && walk) //si el cursor pasa por el objeto
        {
            textBox.SetActive(true);
            TextManager.SetActive(false); //para que no de error ya que no está usando texto independiente del textboxmanager
            text.text = "Salir del sotano";

            //puertaText.SetActive(true); //se ve el texto
            if (Input.GetMouseButtonDown(0))//si das click derecho
            {
                GameObject.Find("VideoBase").GetComponent<Animation>().Play("FADETOWHITEIN");

                otherScene = STREETONE; 
                GameObject.Find("Background").GetComponent<Image>().sprite = otherScene;
                GameObject.Find("Basementv2").SetActive(false);

                GameObject.Find("VideoBase").GetComponent<Animation>().Play("FADETOWHITEOUT");

            }

        }

        //para abrir/cerrar el inventario
        else if (menuColl.OverlapPoint(mousePos))
        {

            if (Input.GetMouseButtonDown(0))//si das click derecho
            {
                if (UiInventory.activeSelf)
                {
                    UiInventory.SetActive(false);
                }
                else
                {
                    UiInventory.SetActive(true);
                }

            }
        }

        else //el raton no está encima de nada importante
        {
            textBox.SetActive(false);
            TextManager.SetActive(true);

        }


    }

    //Para saber si se está en medio de una interacción con otro objeto (de momento aqui no lo he usado pero sería
    //añadir la variable al if de dar click derecho como otra comprobacion 
    //bool desocupado()
    //{
    //    if (textBoxManagerNPC.activeSelf||textBoxManagerObj.activeSelf) //si el resto están descativados (esto es parte del viejo codigo, habria que cmabiarlo)
    //      { return false; }
    //    else { return true; }

    //}


}

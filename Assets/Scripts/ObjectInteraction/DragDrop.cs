﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    Vector2 _startPosition;
    Transform _originalParent;

    private float minX = 10;
    private float maxX = 10.4f;
    private float minY = -6;
    private float maxY = 6;


    //Item item1;
    //Item item2;

    Vector2 mousePos;
    Vector2 itemPos;
    public UI_Inventory UI_inventory;

    public ItemAssets itemAssets;
    private Item item;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();

    }
    private void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //coge la posicion del mouse
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GameObject itemObject = this.gameObject;

        if (eventData.button == PointerEventData.InputButton.Left && itemObject!=null)
        {
           // Debug.Log("OnPointerDown");
            //Debug.Log(this.gameObject.name + "is clicked" + this.gameObject.GetInstanceID());

            itemPos = itemObject.GetComponent<RectTransform>().localPosition;
        }

        else if (eventData.button == PointerEventData.InputButton.Right && itemObject != null)
        {
            //Debug.Log("RightClick");

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);
            

            
           // Debug.Log("collision item " + itemObject);
           // Debug.Log(itemObject.name == "ItemSlotTemplate(Clone)");
            if (itemObject.name == "ItemSlotTemplate(Clone)")
            {
             //   Debug.Log("Entra en el bucle de colision");

                Sprite itemSprite = itemObject.transform.Find("Image").GetComponent<Image>().sprite; //Coge el sprite del subobjeto imagen
              //  Debug.Log("itemSprite golpeado es " + itemSprite);

                string itemType = itemAssets.GetSprite(itemSprite); //comprueba qué tipo de objeto es comparando la imagen con las de ItemAssets

                Debug.Log("El tipo de objeto es" + itemType);

                //Dependiendo del tipo de objeto que era la imagen crea un objeto de dicho tipo
                if (itemType == "Neoprene") { item = new Item { itemType = Item.ItemType.Neoprene, amount = 1 }; }
                else if (itemType == "LargeCan") { item = new Item { itemType = Item.ItemType.LargeCan, amount = 1 }; }
                else if (itemType == "HairTie") { item = new Item { itemType = Item.ItemType.HairTie, amount = 1 }; }
                else if (itemType == "Wool") { item = new Item { itemType = Item.ItemType.Wool, amount = 1 }; }
                else if (itemType == "Coin") { item = new Item { itemType = Item.ItemType.Coin, amount = 1 }; }
                else if (itemType == "Juggle") { item = new Item { itemType = Item.ItemType.Juggle, amount = 1 }; }
                else if (itemType == "Stick") { item = new Item { itemType = Item.ItemType.Stick, amount = 1 }; }

                    
                //Debug.Log("Usando objeto" + item.itemType);
                //Debug.Log("OnButtonCLick");

                //Envia el objeto al OnButtonClick del script UI_Inventory (que lo he modificado para que acepte el argumento y así sepa qué objeto tiene que usar)
                UI_inventory.onButtonClick(item);

            }


                
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.SetParent(canvasGroup.transform);

        _startPosition = transform.position;
        _originalParent = transform.parent;

        //Debug.Log("OnBeginDrag");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;

        //esto sería para cambiar luego dos items de posicion pero está en proceso
        //item1 = new Item { itemType = item1.itemType, amount = item1.amount };




    }

    public void OnDrag(PointerEventData eventData)
    {
     //   Debug.Log("OnDrag " + mousePos);

        mousePos.x = Mathf.Clamp(mousePos.x, minX, maxX);
        mousePos.y = Mathf.Clamp(mousePos.y, minY, maxY);

        //Lo comentado era para que no se moviese fuera de la caja pero se buggea (creo que a lo mejor choca con los otros colliders)//

        //if (mousePos.x > minX && mousePos.x < maxX && mousePos.y > minY && mousePos.y < maxY)
        //{
            //transform.position = eventData.position; //esto lo he cogido de otro tutorial pero hace que desaparezca la imagen lol
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        //}


    }

    public void OnEndDrag(PointerEventData eventData)
    {
       // Debug.Log("OnEndDrag");
        transform.position = _startPosition;
        transform.SetParent(_originalParent);

        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;













    }








}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Item {

    public enum ItemType {
        Neoprene,
        LargeCan,
        HairTie,
        Coin,
        Wool,
        Juggle,
        Stick,
    }

    public ItemType itemType;
    public int amount;


    public Sprite GetSprite() {
        switch (itemType) {
        default:
        case ItemType.Neoprene:     return ItemAssets.Instance.neopreneSprite;
        case ItemType.LargeCan:     return ItemAssets.Instance.largeCanSprite;
        case ItemType.HairTie:      return ItemAssets.Instance.hairTieSprite;
        case ItemType.Coin:         return ItemAssets.Instance.coinSprite;
        case ItemType.Wool:         return ItemAssets.Instance.woolSprite;
        case ItemType.Juggle:       return ItemAssets.Instance.juggleSprite;
        case ItemType.Stick:        return ItemAssets.Instance.stickSprite;
        }
    }

    public bool IsStackable() {
        switch (itemType) {
        default:
        case ItemType.Coin:
        case ItemType.Stick:
        case ItemType.Juggle:
            return true;
        case ItemType.HairTie:
        case ItemType.LargeCan:
        case ItemType.Wool:
        case ItemType.Neoprene:
            return false;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemWorld : MonoBehaviour {

    public static ItemWorld SpawnItemWorld(Vector3 position, Item item) {
        Transform transform = Instantiate(ItemAssets.Instance.pfItemWorld, position, Quaternion.identity);
        
        ItemWorld itemWorld = transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(item);

        itemWorld.transform.SetParent(GameObject.Find("CanvasUI").transform, false);

        return itemWorld;
    }

    private Item item;
    //private SpriteRenderer spriteRenderer;
    private Text text;

    private void Awake() {
        //spriteRenderer = GetComponent<SpriteRenderer>();
        text = transform.Find("AmountText").GetComponent<Text>();
    }

    public void SetItem(Item item) {
        this.item = item;
        gameObject.GetComponent<Image>().sprite = item.GetSprite();
        if (item.amount > 1) {
            text.text = item.amount.ToString();
        } else {
            text.text = "";
        }
    }

    public Item GetItem() {
        return item;
    }

    public void DestroySelf() {
        Destroy(gameObject);
    }

}

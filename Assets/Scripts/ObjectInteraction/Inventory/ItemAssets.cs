﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAssets : MonoBehaviour {

    public static ItemAssets Instance { get; private set; }

    private void Awake() {
        Instance = this;
    }


    public Transform pfItemWorld;

    public Sprite neopreneSprite;
    public Sprite largeCanSprite;
    public Sprite hairTieSprite;
    public Sprite woolSprite;
    public Sprite coinSprite;
    public Sprite juggleSprite;
    public Sprite stickSprite;

    //Metodo para poder saber si el sprite del objeto que clicas es de un objeto u otro (lo uso en DragDrop)
    public string GetSprite(Sprite spr)
    {
        //Debug.Log("nombre del sprite es " +spr.name);
        if (spr.name == neopreneSprite.name) { return "Neoprene"; }
        else if (spr.name == largeCanSprite.name) { return "LargeCan"; }
        else if (spr.name == hairTieSprite.name) { return "HairTie"; }
        else if (spr.name == woolSprite.name) { return "Wool"; }
        else if (spr.name == coinSprite.name) { return "Coin"; }
        else if (spr.name == juggleSprite.name) { return "Juggle"; }
        else if (spr.name == stickSprite.name) { return "Stick"; }
        else return "nada";

    }

}

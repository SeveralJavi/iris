﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory {

    public event EventHandler OnItemListChanged;

    private List<Item> itemList;
    private Action<Item> useItemAction;



    public Inventory(Action<Item> useItemAction) {
        Debug.Log("Action<Item>Use Action");
        this.useItemAction = useItemAction; //El proceso pasa al Player Controller
        itemList = new List<Item>();

        AddItem(new Item { itemType = Item.ItemType.Coin, amount = 1 });
        AddItem(new Item { itemType = Item.ItemType.Coin, amount = 1 });
        AddItem(new Item { itemType = Item.ItemType.Stick, amount = 1 });
        AddItem(new Item { itemType = Item.ItemType.Neoprene, amount = 1 });
        AddItem(new Item { itemType = Item.ItemType.Wool, amount = 1 });
    }

    public void AddItem(Item item) {
        if (item.IsStackable()) {
            bool itemAlreadyInInventory = false;
            foreach (Item inventoryItem in itemList) {
                Debug.Log("Stackable; SUMANDO UNIDAD DE " + item.itemType);
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount += item.amount;
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory) {
                Debug.Log("Stackable; INTEGRANDO " + item.itemType);
                itemList.Add(item);
            }
        } else {
            Debug.Log("No stackable; INTEGRANDO " + item.itemType);
            itemList.Add(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveItem(Item item) {
       // Debug.Log("Remove "+item);
        Item itemInInventory = null;

        //Debug.Log("itemInventory a null");
        for (int i = 0; i < itemList.Count; i++)
        {
            Item inventoryItem = itemList[i];
            if (inventoryItem.itemType == item.itemType)
            {
              //  Debug.Log("Stackable; SACANDO UNIDAD DE " + item.itemType);
                inventoryItem.amount -= item.amount;
                itemInInventory = inventoryItem;
               // Debug.Log("itemInInventory (loop stackable) es " + itemInInventory.itemType + "y hay " + itemInInventory.amount);
            }
        }
        if (itemInInventory != null && itemInInventory.amount <= 1)
        {
           // Debug.Log("No stackable; ELIMINANDO " + item.itemType);
            itemList.Remove(itemInInventory);
        }

        else
        {
          //  Debug.Log("else remove ");
            itemList.Remove(item);
        }

        /*
        if (item.IsStackable())
        {

            for (int i = 0; i < itemList.Count; i++)
            {
                Item inventoryItem = itemList[i];
                if (inventoryItem.itemType == item.itemType)
                {
                    Debug.Log("Stackable; SACANDO UNIDAD DE " + item.itemType);
                    inventoryItem.amount -= item.amount;
                    itemInInventory = inventoryItem;
                    Debug.Log("itemInInventory (loop stackable) es " + itemInInventory.itemType + "y hay "+itemInInventory.amount);
                }
            }

        }
        //Debug.Log(itemInInventory.itemType+ "es itemInInventory");
        //Debug.Log("y su cantidad es " + itemInInventory.amount);

        if (itemInInventory != null && itemInInventory.amount <= 1)
        {
            Debug.Log("No stackable; ELIMINANDO " + item.itemType);
            itemList.Remove(itemInInventory);
        }

        else
        {
            Debug.Log("else remove ");
            itemList.Remove(item); 
        }

        Debug.Log("invoke");
        ESTO ES LO QUE HACE SALTAR ERROR CREO
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
        */
    }

    public void UseItem(Item item) {
       // Debug.Log("Ha llegado al UseItem de Inventory con el item "+item.itemType);
        useItemAction(item);
    }

    public List<Item> GetItemList() {
        return itemList;
    }

}

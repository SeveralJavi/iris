﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Inventory : MonoBehaviour {

    private Inventory inventory;
    private Transform itemSlotContainer;
    private Transform itemSlotTemplate;
    private PlayerController player;
    private bool buttonClicked = false;

    private Item itemForUse = null;

    //Para poder cambiar luego dos items en el inventario de posicion, en proceso
    public void MoveItem(Transform item1, Transform item2 )
    {
        //InventorySlot temp = new InventorySlot(item2.item, item2.amount);
        //item2.UpdateSlot(item1.item, item1.amount);
        //item1.UpdateSlot(temp.item, temp.amount);
    }



    private void Awake() {
        itemSlotContainer = transform.Find("ItemSlotContainer");
        itemSlotTemplate = itemSlotContainer.Find("ItemSlotTemplate");
    }

    public void SetPlayer(PlayerController player) {
        this.player = player;
    }

    public void SetInventory(Inventory inventory) {
        this.inventory = inventory;

        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e) {
        RefreshInventoryItems();
    }

    public void RefreshInventoryItems() {
        foreach (Transform child in itemSlotContainer) {
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }

        float x = 0;
        float y = 2.5f;
        float itemSlotCellSize = 57f;
        
        foreach (Item item in inventory.GetItemList().ToArray()) {

            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            if (buttonClicked)//&&itemForUse!=null)
            {
                //Debug.Log("Bucle foreach de UI, usando" +itemForUse.itemType);

                inventory.UseItem(itemForUse);     //se usa el tipo de objeto especificado en el OnButton (el proceso pasa al script Inventory)
                buttonClicked = false;
                //itemForUse = null;
            }

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, y * itemSlotCellSize);
            Image image = itemSlotRectTransform.Find("Image").GetComponent<Image>();
            image.sprite = item.GetSprite();

            Text uiText = itemSlotRectTransform.Find("AmountText").GetComponent<Text>();
            if (item.amount > 1) {
                uiText.text = item.amount.ToString();
            } else {
                uiText.text = "";
            }

            y--;
            if (y >= 4) {
                y = 0;
                x--;
            }
        }
    }

    //He modificado un poco este metodo para que acepte un Item 
    public void onButtonClick(Item itemUse)
    {
        itemForUse = itemUse;
        //Debug.Log("ha llegado al onbutton con" +itemForUse.itemType);
        buttonClicked = true;
        RefreshInventoryItems();
    }
}

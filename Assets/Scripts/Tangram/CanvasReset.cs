﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasReset : MonoBehaviour
{

    private static CanvasReset canvas;


    private void Awake()
    {
        MakeThisTheOnlyCanvas();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void MakeThisTheOnlyCanvas()
    {
        if (canvas == null)
        {
            DontDestroyOnLoad(gameObject);
            canvas = this;
        }
        else
        {
            if (canvas != this)
            {
                Destroy(gameObject);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinMechanic : MonoBehaviour
{

    private Rigidbody2D rb;
    private SpringJoint2D union;

    Collider2D polygonCollider;

    List<GameObject> PiecePlaces = null;

    private float deltaX, deltaY; // Calculan un offset entre el centro del objeto y la posicion del touch
    public static bool joined = false;

    GameController gameControllerInstance;


    public PieceList allPieces;

    GameObject piece;


    // Start is called before the first frame update
    void Start()
    { 
        gameControllerInstance = FindObjectOfType<GameController>();

        PiecePlaces = allPieces.obtainList();

    }

    // Update is called once per frame
    void Update()
    {
        if (gameControllerInstance.onJoinMode)
        {


            Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {

                foreach (GameObject possiblePiece in PiecePlaces)
                {
                    if (Mathf.Abs(clickPos.x - possiblePiece.transform.position.x) <= 0.5f &&
                       Mathf.Abs(clickPos.y - possiblePiece.transform.position.y) <= 0.5f)
                    {
                        polygonCollider = possiblePiece.GetComponent<Collider2D>();
                        break;
                    }

                }

                if (polygonCollider == Physics2D.OverlapPoint(clickPos))
                {


                    if (gameControllerInstance.onJoinMode && polygonCollider != null)
                    {
                        joined = true;
                        deltaX = clickPos.x - polygonCollider.transform.position.x; // Definición del offset
                        deltaY = clickPos.y - polygonCollider.transform.position.y;

                    }
                }


            }

            if (joined)
            {
                polygonCollider.transform.position = new Vector2(clickPos.x - deltaX, clickPos.y - deltaY);
            }

            if (Input.GetMouseButtonUp(0))
            {

                joined = false;

            }
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        

        if (gameControllerInstance.onJoinMode)
        {
              

            gameObject.transform.SetParent(collision.transform);

            rb = gameObject.GetComponent<Rigidbody2D>();
            
            rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
            
            
            gameControllerInstance.activateMovement();
            
        }
    }
}

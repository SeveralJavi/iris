﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CropMechanic : MonoBehaviour
{

     private GameController gameControllerInstance;
     Collider2D polygonCollider;

     private List<GameObject> PiecePlaces = null;

     public PieceList allPieces;

     GameObject piece;




    // Start is called before the first frame update
    void Start()
    {
        gameControllerInstance = FindObjectOfType<GameController>();
      

        PiecePlaces = allPieces.obtainList();

    }

    // Update is called once per frame
    void Update()
    {
        if (gameControllerInstance.onCropMode)
        {
            Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {
                foreach (GameObject possiblePiece in PiecePlaces)
                {

                    if (possiblePiece.GetComponent<Collider2D>() == Physics2D.OverlapPoint(clickPos))
                    {
                        Debug.Log("Esta opcion es mejor");
                        polygonCollider = possiblePiece.GetComponent<Collider2D>();
                        piece = possiblePiece;
                        break;


                    }

                }

                if (polygonCollider == Physics2D.OverlapPoint(clickPos))
                {
                    if (gameControllerInstance.onCropMode)
                    {

                        int numberChildren = piece.transform.childCount;
                        Debug.Log(numberChildren + "dentro");

                        bool normalCrop;

                        if (piece.transform.parent == null)
                        {
                            normalCrop = delete(piece);
                        }
                        else
                        {
                            normalCrop = delete(piece.transform.parent.gameObject);
                            
                        }



                        if (normalCrop)
                        {
                            Debug.Log("HE ENTRADO");
                            if (piece.transform.childCount != 0)
                            {
                                piece.gameObject.SetActive(false);
                            }

                            foreach (Transform eachChild in piece.transform)
                            {
                                eachChild.parent = eachChild.parent.parent;
                                eachChild.gameObject.SetActive(true);
                            }
                        }


                    }
                }


            }
            

        }

       

        bool delete(GameObject parent)
        {
            bool cropMode = true;

            for (int i = 0; i < parent.transform.childCount; i++)
            {
                if (parent.transform.GetChild(i).gameObject.activeInHierarchy)
                {
                    cropMode = false;
                    parent.transform.GetChild(i).transform.parent = null;
                    Debug.Log("HOLAAAAAAAAAA");
                }
            }
            return cropMode;
        }
    }
}

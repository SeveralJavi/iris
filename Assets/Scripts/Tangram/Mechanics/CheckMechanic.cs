﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class CheckMechanic : MonoBehaviour
{



    List<GameObject> pieceTemplates;  //Piezas que conforman el puzzle1
    List<GameObject> pieceTemplates2;  //Piezas que conforman el puzzle2
    List<GameObject> piecePlaces;      //Piezas que el jugador va a poder mover
    List<GameObject> piecePlacesBlocked;  //Piezas encajadas en el puzzle1
    List<GameObject> piecePlacesBlocked2; //Piezas encajadas en el puzzle2

    public PieceList allPiece;          //Obtenemos el script donde estan almacenados los datos de las piezas

    public PieceTemplateList allPiecesTemplates;   //Obtenemos el script donde estan almacenados las piezas que conforman los puzzles

    public GameObject image;                    //Imagen del objeto 1

    public GameObject image1;

    Collider2D polygonCollider;

    GameObject piece, pieceTemplate;

    GameController gameControllerInstance;       //Coordenadas raton

    bool showed = false;                        //Con este bool sabremos si la imagen del puzzle completado ya ha aparecido

    SpriteRenderer rend, rend1;                      //SpriteRenderer de la imagen que va a aparecer

    public GameObject clickEffect;              //Efecto al clickar

    public RawImage rawImage;                  //Estos dos siguientes objetos nos permitiran hacer el Fade a blanco al cambiar de escena
    public GameObject myVideoBase;
     
    bool fadeDone = false;                      //Con este bool sabremos si el Fade ha finalizado

    bool changingScene = false;                 //Con este bool sabremos si ya se ha cambiado de escena

    bool tangram1, tangram2 = false;

    public Image white;


    private void Awake()
    {
        myVideoBase.GetComponent<Animation>().Play("FADETOWHITEOUT");
    }

    // Start is called before the first frame update
    void Start()
    {
        piecePlaces = allPiece.obtainList();              //Obtengo la lista de las piezas que el jugador va a poder mover, rotar, etc...
        pieceTemplates = allPiecesTemplates.obtainList(); //Obtengo la lista de las piezas puzle 1
        pieceTemplates2 = allPiecesTemplates.obtainList2(); //Obtengo la lista de las piezas puzle 2
        piecePlacesBlocked = allPiece.obtainListBlocked();  //Obtengo la lista de las piezas que están bloqueadas
        piecePlacesBlocked2 = allPiece.obtainListBlocked2();

        gameControllerInstance = FindObjectOfType<GameController>();   //Encuentro el "mouse"

        rend = image.GetComponent<SpriteRenderer>();   //Estas 4 lineas son para coger los componentes necesarios (y configurarlos) para que aparezca                                                             
        Color c = rend.material.color;                 //la imagen del objeto que hemos solucionado.
        c.a = 0f;
        rend.material.color = c;

        rend1 = image1.GetComponent<SpriteRenderer>();
        c = rend1.material.color;
        c.a = 0f;
        rend1.material.color = c;

        myVideoBase = GameObject.Find("VideoBase");                                                  //Estas dos lineas nos sirven para hacer el Fade a blanco
        myVideoBase.GetComponent<RawImage>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);     //al hacer el cambio de escena.


    }
    // Update is called once per frame
    void Update()
    {
        if (gameControllerInstance.onMoveMode)            //Esto hace que las piezas unicamente se puedan encajar cuando el jugador esta moviendo una pieza.
        {


            
            Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);        //Obtengo la posicion del mouse



            if (Input.GetMouseButtonDown(0))                          //Clicko
            {
                foreach (GameObject possiblePiece in piecePlaces)
                {
                    if (Mathf.Abs(clickPos.x - possiblePiece.transform.position.x) <= 0.5f &&
                       Mathf.Abs(clickPos.y - possiblePiece.transform.position.y) <= 0.5f)    //Busco mediante posicion que pieza checkear
                    {
                        polygonCollider = possiblePiece.GetComponent<Collider2D>();
                        piece = possiblePiece;
                        break;
                    }
                }

            }

            


            //PUZZLE 1

            if (piece != null && Input.GetMouseButtonUp(0))            //En el caso de que haya clickado sobre una pieza comprobamos si esta en alguna posición correcta
            {
                foreach (GameObject possiblePieceTemplate in pieceTemplates)
                {
                    if (Mathf.Abs(piece.transform.position.x - possiblePieceTemplate.transform.position.x) <= 0.5f && Mathf.Abs(piece.transform.position.y - possiblePieceTemplate.transform.position.y) <= 0.5f)
                    {
                        if (System.Math.Round(piece.transform.eulerAngles.z, 2) == System.Math.Round(possiblePieceTemplate.transform.eulerAngles.z, 2))
                        {

                            if (System.Math.Round(piece.transform.localScale.z, 2) == System.Math.Round(possiblePieceTemplate.transform.localScale.z, 2) &&
                                System.Math.Round(piece.transform.localScale.y, 2) == System.Math.Round(possiblePieceTemplate.transform.localScale.y, 2) &&
                                System.Math.Round(piece.transform.localScale.x, 2) == System.Math.Round(possiblePieceTemplate.transform.localScale.x, 2))
                            {
                                pieceTemplate = possiblePieceTemplate;
                                break;
                            }
                        }
                    }
                }



                if (pieceTemplate != null && !Collision(piece))   //Este if comprueba si la pieza esta en una posicion correcta
                {                                                 //El metodo Collision() nos permite solucionar un bug que hacia que algunas piezas se colocaran unas encima de otras

                    piece.transform.position = new Vector2(pieceTemplate.transform.position.x, pieceTemplate.transform.position.y); //
                    piece.GetComponent<SpriteRenderer>().sortingLayerName = "PieceColocated";

                    if (!piecePlacesBlocked.Contains(piece))
                        piecePlacesBlocked.Add(piece);
                    piecePlaces.Remove(piece);
                    piece.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                    piece.GetComponent<PolygonCollider2D>().isTrigger = true;



                    GameObject newClickEffect = (GameObject)Instantiate(clickEffect, transform.position, Quaternion.identity);
                    Destroy(newClickEffect, 1);




                    piece = null;
                    pieceTemplate = null;

                }


            
                pieceTemplate = null;


            }


            //PUZZLE 2

            if (piece != null && Input.GetMouseButtonUp(0))            //En el caso de que haya clickado sobre una pieza comprobamos si esta en alguna posición correcta
            {
                Debug.Log("NIVEL 1");
                foreach (GameObject possiblePieceTemplate in pieceTemplates2)
                {
                    if (Mathf.Abs(piece.transform.position.x - possiblePieceTemplate.transform.position.x) <= 0.5f && Mathf.Abs(piece.transform.position.y - possiblePieceTemplate.transform.position.y) <= 0.5f)
                    {
                        if (System.Math.Round(piece.transform.eulerAngles.z, 2) == System.Math.Round(possiblePieceTemplate.transform.eulerAngles.z, 2))
                        {

                            if (System.Math.Round(piece.transform.localScale.z, 2) == System.Math.Round(possiblePieceTemplate.transform.localScale.z, 2) &&
                                System.Math.Round(piece.transform.localScale.y, 2) == System.Math.Round(possiblePieceTemplate.transform.localScale.y, 2) &&
                                System.Math.Round(piece.transform.localScale.x, 2) == System.Math.Round(possiblePieceTemplate.transform.localScale.x, 2))
                            {
                                pieceTemplate = possiblePieceTemplate;
                                break;
                            }
                        }
                    }
                }




                if (pieceTemplate != null && !Collision2(piece))   //Este if comprueba si la pieza esta en una posicion correcta
                {                                                 //El metodo Collision() nos permite solucionar un bug que hacia que algunas piezas se colocaran unas encima de otras

                    piece.transform.position = new Vector2(pieceTemplate.transform.position.x, pieceTemplate.transform.position.y); //
                    piece.GetComponent<SpriteRenderer>().sortingLayerName = "PieceColocated";

                    if (!piecePlacesBlocked2.Contains(piece))
                        piecePlacesBlocked2.Add(piece);
                    piecePlaces.Remove(piece);
                    piece.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                    piece.GetComponent<PolygonCollider2D>().isTrigger = true;




                    GameObject newClickEffect = (GameObject)Instantiate(clickEffect, transform.position, Quaternion.identity);
                    Destroy(newClickEffect, 1);




                    piece = null;
                    pieceTemplate = null;

                }



                pieceTemplate = null;


            }


            if (piecePlacesBlocked.Count == 1 && showed == false)
            {

                StartCoroutine(FadeIn(rend));
                showed = true;
                tangram1 = true;

               


            }
            
            else if(piecePlacesBlocked2.Count == 1 && showed == false)
            {
                StartCoroutine(FadeIn(rend1));
                showed = true;
                tangram2 = true;


            }
           
            if (fadeDone && showed && Input.GetMouseButtonDown(0))
            {
                myVideoBase.transform.SetAsLastSibling();
                myVideoBase.GetComponent<Animation>().Play("FADETOWHITEIN");
              
                fadeDone = false;
                changingScene = true;

               



            }
            
            if (myVideoBase.GetComponent<Animation>().isPlaying == false && changingScene)
            {

               // GameObject.Destroy(GameObject.Find("CanvasTangram"));
                //GameObject.Destroy(GameObject.Find("GameControlTangram"));
               
                if (tangram1)
                {
               
                    SceneManager.LoadScene("Scene1.1", LoadSceneMode.Single);
                 

                }
                if (tangram2)
                    SceneManager.LoadScene("Scene1.2", LoadSceneMode.Single);

            }
            



        }

    }

    

    private bool Collision(GameObject piece)
    {
        foreach(GameObject pieceblocked in piecePlacesBlocked)
        {
            if (Mathf.Abs(piece.transform.position.x - pieceblocked.transform.position.x) <= 0.5f && Mathf.Abs(piece.transform.position.y - pieceblocked.transform.position.y) <= 0.5f)
            {
                return true;
            }
        }
        return false;
    }
    private bool Collision2(GameObject piece)
    {
        foreach (GameObject pieceblocked in piecePlacesBlocked2)
        {
            if (Mathf.Abs(piece.transform.position.x - pieceblocked.transform.position.x) <= 0.5f && Mathf.Abs(piece.transform.position.y - pieceblocked.transform.position.y) <= 0.5f)
            {
                return true;
            }
        }
        return false;
    }




    private IEnumerator FadeIn(SpriteRenderer rend)
    {
        for(float f = 0.02f; f<=1; f+= 0.02f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend.material.color = c;
            yield return new WaitForSeconds(0.025f);
        }

        fadeDone = true;
        

    }

 






}

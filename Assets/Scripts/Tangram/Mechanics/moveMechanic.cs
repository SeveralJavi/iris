﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveMechanic : MonoBehaviour
{
   
    private List<GameObject> PiecePlaces = null;
    private List<GameObject> PiecePlacesBlocked = null;
    private List<GameObject> PiecePlacesBlocked2 = null;

    private Vector2 initialPosition;
    GameController gameControllerInstance;
    Collider2D polygonCollider;
    private float deltaX, deltaY; // Calculan un offset entre el centro del objeto y la posicion del touch
    public static bool selected = false;
    Vector3 lastMouseCoordinate = Vector3.zero;

    public PieceList allPieces;

    GameObject piece;




    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.position;
        gameControllerInstance = FindObjectOfType<GameController>();

        PiecePlaces = allPieces.obtainList();
        PiecePlacesBlocked = allPieces.obtainListBlocked();
        PiecePlacesBlocked2 = allPieces.obtainListBlocked2();



    }

    // Update is called once per frame
    void Update()
    {

        if (gameControllerInstance.onMoveMode)
        {
            Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //Para saber posicion actual del 


            Vector3 mouseDelta = Input.mousePosition - lastMouseCoordinate;

            if (Input.GetMouseButtonUp(1))          //Esto es para desbloquear piezas
            {
                foreach (GameObject pieceBlocked in PiecePlacesBlocked)
                {
                    if (pieceBlocked.GetComponent<Collider2D>() == Physics2D.OverlapPoint(clickPos))
                    {
                        PiecePlaces.Add(pieceBlocked);
                        Debug.Log("Hola"); 
                        
                        pieceBlocked.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        pieceBlocked.GetComponent<SpriteRenderer>().sortingLayerName = "Pieces";
                        PiecePlacesBlocked.Remove(pieceBlocked);
                        break;
                    }
                }


                foreach (GameObject pieceBlocked in PiecePlacesBlocked2)
                {
                    if (pieceBlocked.GetComponent<Collider2D>() == Physics2D.OverlapPoint(clickPos))
                    {
                        PiecePlaces.Add(pieceBlocked);
                        Debug.Log("Hola");

                        pieceBlocked.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        pieceBlocked.GetComponent<SpriteRenderer>().sortingLayerName = "Pieces";
                        PiecePlacesBlocked2.Remove(pieceBlocked);
                        break;
                    }
                }
            }


            if (Input.GetMouseButtonDown(0))
            {

                foreach (GameObject possiblePiece in PiecePlaces)
                {
                    if(possiblePiece.GetComponent<Collider2D>() == Physics2D.OverlapPoint(clickPos))
                    { 
                        if (possiblePiece.transform.parent == null)
                        {
                            piece = possiblePiece;
                            polygonCollider = possiblePiece.GetComponent<Collider2D>();
                            Debug.Log("No tiene padre");
                        }
                        else
                        {
                            piece = possiblePiece.transform.parent.gameObject;
                            polygonCollider = GameObject.Find(piece.transform.name).GetComponent<Collider2D>();
                            Debug.Log("TIENE PADRE" + piece.transform.name);


                        }
                        break;
                    }
                }


                if (polygonCollider == Physics2D.OverlapPoint(clickPos))    //Primer bug arreglado --> Borrar cuando sepamos que no le damos uso.
                {

                    if (gameControllerInstance.onMoveMode && polygonCollider != null)
                    {
                        {
                            deltaX = clickPos.x - polygonCollider.transform.position.x; // Definición del offset
                            deltaY = clickPos.y - polygonCollider.transform.position.y;
                            selected = true;
                        }
                    }



                }

                if (PiecePlaces.Contains(piece) == false)
                {
                    selected = false;
                }


            }

            if (selected)
            {
                polygonCollider.transform.position = new Vector2(clickPos.x - deltaX, clickPos.y - deltaY);
            }

            lastMouseCoordinate = Input.mousePosition;


            if (Input.GetMouseButtonUp(0))
            {
                selected = false;

            }



        }

    }
    

}

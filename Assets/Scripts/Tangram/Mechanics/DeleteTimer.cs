﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeleteTimer : MonoBehaviour
{

    public string scene;

    public RawImage rawImage;                  //Estos dos siguientes objetos nos permitiran hacer el Fade a blanco al cambiar de escena
    public GameObject myVideoBase;
    // Start is called before the first frame update
    void Awake()
    {
        
       if (GameObject.Find("CanvasTangram"))
        {

            GameObject.Destroy(GameObject.Find("GameControlTangram"));
            GameObject.Destroy(GameObject.Find("CanvasTangram"));

           
            myVideoBase.GetComponent<RawImage>().material.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            myVideoBase.GetComponent<Animation>().Play("FADETOWHITEOUT");

            //myVideoBase.gameObject.SetActive(false);
        }

    }



}

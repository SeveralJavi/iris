﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoteMechanic : MonoBehaviour
{
    
    List<GameObject> PiecePlaces;

    GameController gameControllerInstance = null;
    Collider2D polygonCollider = null;
    GameObject piece = null;
    public PieceList allPieces = null;
    public static bool selected = false;

    bool continueRotating = true;  //Nos indica cuando hay que seguir rotando y cuando no

    public float timeRemaining = 0.5f; //Es el tiempo de espera cuando llegue a los angulos marcados como clave

    List<float> grades = new List<float> { 0.0f, 45.0f, 90.0f, 135.0f, 180.0f, 225.0f, 270.0f, 315.0f }; //Son los angulos donde la pieza parará


    // Start is called before the first frame update
    void Start()
    {
        gameControllerInstance = FindObjectOfType<GameController>();
        polygonCollider = null;


        PiecePlaces = allPieces.obtainList(); //Obtengo la lista de las piezas en tablero
    }

    // Update is called once per frame
    void Update()
    {
        if (gameControllerInstance.onRotateMode)
        {
            Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            foreach (GameObject possiblePiece in PiecePlaces)
            {
                if (possiblePiece.GetComponent<Collider2D>() == Physics2D.OverlapPoint(clickPos))   //Busco mediante posicion que pieza es la que tiene que rotar
                {
                    polygonCollider = possiblePiece.GetComponent<Collider2D>();
                    piece = possiblePiece;
                    break;


                }
            }
            if (Input.GetMouseButtonDown(0))
            {

                if (polygonCollider == Physics2D.OverlapPoint(clickPos))
                {
                    if (gameControllerInstance.onRotateMode && piece != null && piece.transform.parent == null) //Compruebo que la pieza esta en rotate mode y que no tiene padre, ya que si tiene Padre, hay que rotar sobre los ejes del padre.
                    {
                        selected = true;

                    }
                }

            }

            if (selected && continueRotating)
            {
                piece.transform.Rotate(1 * Vector3.forward, Space.World);    //Roto la pieza, en cada Update rota un grado
                                                                             //Debug.Log(Mathf.Round(piece.transform.eulerAngles.z));        //Comprobacion de que rota
                if (grades.Contains(Mathf.Round(piece.transform.eulerAngles.z)))
                {        //Compruebo si la pieza ha rotado a algun angulo clave
                    Debug.Log("LO DETECTA");
                    continueRotating = false;                                 //False ya que tiene que parar de rotar
                }

            }

            checkRotation();                                                  //Checkeo la rotacion

            if (Input.GetMouseButtonUp(0))
            {

                selected = false;

            }


            void checkRotation()
            {
                if (continueRotating == false)                     //Si la pieza se encuentra en un angulo clave, comenzará un contador durante el cual estará parada.
                {
                    timeRemaining -= Time.deltaTime;
                }
                if (timeRemaining < 0.1f)                        //Si el contador llega a 0, la pieza volverá a rotar.
                {
                    continueRotating = true;
                    timeRemaining = 0.8f;
                }

            }
        }
    }
}



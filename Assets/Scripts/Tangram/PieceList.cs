﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceList : MonoBehaviour
{
    public List<GameObject> allPiecePlaces = null;

    public List<GameObject> allPiecePlacesBlocked = null;

    public List<GameObject> allPiecePlacesBlocked2 = null;

   // public bool Triangle;
   // public bool Square;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public List<GameObject> obtainList()
    {
        return allPiecePlaces;
    }

    public List<GameObject> obtainListBlocked()
    {
        return allPiecePlacesBlocked;
    }

    public List<GameObject> obtainListBlocked2()
    {
        return allPiecePlacesBlocked2;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class OrangePiece : MonoBehaviour
{
    [SerializeField]
    private List<Transform> orangePiecePlaces = null;
    private Vector2 initialPosition;
    Vector3 lastMouseCoordinate = Vector3.zero;
    GameController gameControllerInstance;
    Collider2D polygonCollider;
    private float deltaX, deltaY; // Calculan un offset entre el centro del objeto y la posicion del touch
    public static bool locked; // Está colocada en la pos final
    public static bool selected; // Está siendo movida

    void Start()
    {
        initialPosition = transform.position;
        gameControllerInstance = FindObjectOfType<GameController>();
        polygonCollider = GetComponent<Collider2D>();
    }

    // UPDATE: ¿ESTÁ LA PIEZA COLOCADA? SI NO, COMPROBAR INTERACCIÓN
    private void Update()
    {
        if (!locked)
        {
            Vector2 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 mouseDelta = Input.mousePosition - lastMouseCoordinate;

            if(Input.GetMouseButtonDown(0))
            {
                if(polygonCollider == Physics2D.OverlapPoint(clickPos)) // La pieza está sobre template
                {
                    if(gameControllerInstance.onRotateMode) RotatePiece(); // Modo Rotación activado
                    
                    else if(gameControllerInstance.onMoveMode) // Modo Movimiento activado
                    {
                        deltaX = clickPos.x - transform.position.x; // Definición del offset
                        deltaY = clickPos.y - transform.position.y;
                        selected = true;
                    }

                    else if(gameControllerInstance.onCropMode)
                    {
                        foreach (Transform eachChild in transform){
                            eachChild.parent = eachChild.parent.parent;
                            eachChild.gameObject.SetActive(true);
                        }

                        GameObject.Find("Orange-Green Piece").GetComponent<OrangeGreenPiece>().isAttached = false;
                        GameObject.Find("Orange-Green Piece").GetComponent<OrangeGreenPiece>().previousRotation = realRotation;
                        
                        GameObject.Find("Orange-Pink Piece").GetComponent<OrangePinkPiece>().isAttached = false;
                        GameObject.Find("Orange-Pink Piece").GetComponent<OrangePinkPiece>().previousRotation = realRotation;
                        
                        transform.gameObject.SetActive(false);
                    }
                }
            }

            if(selected) transform.position = new Vector2(clickPos.x - deltaX, clickPos.y - deltaY); // Mover pieza

            lastMouseCoordinate = Input.mousePosition;

            if(Input.GetMouseButtonUp(0))
            {
                foreach(Transform posiblePlace in orangePiecePlaces)
                {
                    if (Mathf.Abs(transform.position.x - posiblePlace.position.x) <= 0.5f &&
                        Mathf.Abs(transform.position.y - posiblePlace.position.y) <= 0.5f && // Usa el offset para comprobar si está cerca
                        values[0] == 1) //... y comprueba que esté en la posición correcta
                        {
                            transform.position = new Vector2(posiblePlace.position.x, posiblePlace.position.y);
                            locked = true;
                        }
                        selected = false;
                }
                if(!locked) transform.position = new Vector2(initialPosition.x, initialPosition.y); // Se devuelve el objeto a la posición inicial
            }
        }

        if (transform.rotation.eulerAngles.z != realRotation) {
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.Euler(0, 0, realRotation), speed);
		}
    }

    // ROTACIÓN DE LAS PIEZAS
    public int[] values; // Refleja los valores correctos/aceptados de encaje
	public float speed; // Velocidad de la animación de rotación; puede configurarse desde inspector
	public float realRotation; // Grados de la rotación actual

	public void RotatePiece()
	{
		realRotation += 90;
		if (realRotation == 360) realRotation = 0;
		RotateValues();
	}

	public void RotateValues()
	{
		int aux = values[0];
		for (int i = 0; i < values.Length-1; i++) {
			values [i] = values [i + 1];
		}
		values[3] = aux;
	}

    //UNION DE LAS PIEZAS

    public void OnTriggerEnter2D(Collider2D piece)
    {
        
        piece.gameObject.transform.parent = gameObject.transform;
    }
}
